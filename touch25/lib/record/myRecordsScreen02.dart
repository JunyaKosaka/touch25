import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http; //httpリクエスト用
import 'dart:async'; //非同期処理用
import 'dart:convert'; //httpレスポンスをJSON形式に変換用
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:device_info_plus/device_info_plus.dart';

// class MyRecordsScreen02 extends StatefulWidget {
//   @override
//   _MyRecordsScreen02State createState() => _MyRecordsScreen02State();
// }

class MyRecordsScreen02 extends StatelessWidget {

  List<double> myRecords = [];

  MyRecordsScreen02({Key? key, required this.myRecords}) : super(key: key);

  // void getData() async {
  //   DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();      // // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
  //   IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
  //   String userId = iosInfo.identifierForVendor ?? "";
  //   // myRecords.clear();
  //   CollectionReference records = FirebaseFirestore.instance.collection('records');
  //   // Call the user's CollectionReference to add a new user
  //   records
  //       .where('user', isEqualTo: userId)
  //       .orderBy('time')
  //       .limit(10)
  //       .get()
  //       .then((QuerySnapshot querySnapshot) {
  //     querySnapshot.docs.forEach((doc) {
  //       myRecords.add(doc["time"]);
  //       print(myRecords.length);
  //       print('userId: ' + doc["user"]);
  //       print('time:  ' + doc["time"].toString());
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.home),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed("/home");
          },
        ),
        title: Text("My Records"),
        backgroundColor: Colors.black54,
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/app-photo-tower.jpg'),
              fit: BoxFit.cover,
            )),
        width: double.infinity,
        child: ListView.builder(
          itemCount: myRecords.length,
          itemBuilder: (context, index) {
            return _buildListTile(index);
          },
        ),
      ),
    );
  }

  Widget _buildListTile(int index) {
    return Container(
      decoration: BoxDecoration(
          border: const Border(
              bottom: const BorderSide(
                color: const Color(0x1e333333),
                width: 1,
              )
          )
      ),
      height: 60,
      child: Column(
        children: [
          _buildTitleRow(index),
        ],
      ),
    );
  }

  Widget _buildTitleRow(int index) {
    return ListTile(
      title: Text(
        "${index+1}:  " + myRecords[index].toString() + " sec",
        style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold),
      ),
      // trailing: Text(userData[index]["addDate"], style: TextStyle(fontSize: 9),),
    );
  }
}
