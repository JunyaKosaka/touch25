import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:device_info_plus/device_info_plus.dart';

class MyRecordsScreen extends StatelessWidget {
  // const MyRecordsScreen({Key? key}) : super(key: key);
  List<int> myRecords = [];

  // MyRecordsScreen({Key? key, required this.myRecords}) : super(key: key);
  // CollectionReference users = FirebaseFirestore.instance.collection('records');
  final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance.collection('records')
      // .where('user', isEqualTo: userId)
      .orderBy('time')
      .limit(10)
      .snapshots();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("records"),
        backgroundColor: Colors.black54,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed("/home");
              },
          ),

        ],
      ),

      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: _usersStream,
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Something went wrong');
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text("Loading");
            }

            return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document, ) {
                Map<String, dynamic> data = document.data()! as Map<String, dynamic>;
                return ListTile(
                  title: Text(data['time'].toString()),
                  // subtitle: Text(data['time'].toString()),
                );
              }).toList(),
            );
          },
        ),
      )
    );
  }
}


