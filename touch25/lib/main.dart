import 'package:flutter/material.dart';

import 'package:touch25/game/gameScreen.dart';
import 'package:touch25/game/readyScreen.dart';
import 'package:touch25/game/resultScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:touch25/record/myRecordsScreen.dart';
import 'package:touch25/record/myRecordsScreen02.dart';

import 'package:touch25/home/homeScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Home',
      home: HomeScreen(),
      routes: {
        "/home": (BuildContext context) => HomeScreen(),
        "/game": (BuildContext context) => GameScreen(),
        "/ready": (BuildContext context) => ReadyScreen(),
        "/record": (BuildContext context) => MyRecordsScreen(),
        // "/record02": (BuildContext context) => MyRecordsScreen02(),

      },
    );
  }
}