import 'package:flutter/material.dart';
import 'package:touch25/game/gameScreen.dart';

import 'package:touch25/game/readyScreen.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:touch25/record/myRecordsScreen02.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  List<double> myRecords = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Home'),
      //   backgroundColor: Colors.black54,
      //   centerTitle: true,
      //   automaticallyImplyLeading: false,
      // ),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/world-photo.jpeg'),
              fit: BoxFit.cover,
            )),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: const Text('my records'),
              style: ElevatedButton.styleFrom(
                primary: Colors.orange,
                onPrimary: Colors.black,
                shape: const StadiumBorder(),
              ),
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                  builder: (context) =>
                  MyRecordsScreen02(myRecords: myRecords,)));
                // Navigator.of(context).pushReplacementNamed("/record02");
              },
            ),
            SizedBox(
              height: 50,
            ),
            ElevatedButton(
              child: const Text('START'),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
                onPrimary: Colors.black,
                shape: const StadiumBorder(),
              ),
              onPressed: () {
                Navigator.of(context).pushReplacementNamed("/ready");
              },
            ),
          ],
        ),
      ),
    ),
    );
  }

  void getData() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();      // // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    String userId = iosInfo.identifierForVendor ?? "";
    // myRecords.clear();

    CollectionReference records = FirebaseFirestore.instance.collection('records');
    // Call the user's CollectionReference to add a new user
    records
        .where('user', isEqualTo: userId)
        .orderBy('time')
        .limit(10)
        .get()
        .then((QuerySnapshot querySnapshot) {
      querySnapshot.docs.forEach((doc) {
        myRecords.add(doc["time"]);
        // print('userId: ' + doc["user"]);
        // print('time:  ' + doc["time"].toString());
      });
    });
  }
}
