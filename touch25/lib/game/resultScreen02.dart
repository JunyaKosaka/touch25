import 'dart:math';

import 'package:flutter/material.dart';

import 'gameScreen.dart';

class ResultScreen02 extends StatefulWidget {

  final double resultTime;
  final int myRank;

  const ResultScreen02({Key? key, required this.resultTime, required this.myRank}) : super(key: key);

  @override
  _ResultScreen02State createState() => _ResultScreen02State();
}

class _ResultScreen02State extends State<ResultScreen02> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Result02"),
        backgroundColor: Colors.black54,
        automaticallyImplyLeading: false,
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.all(50.0),
            child: Container(
                margin: const EdgeInsets.all(10.0),
                width: 300,
                height: 50,
                child: Text(
                  'あなたは${widget.resultTime}秒です！',
                  style: TextStyle(fontSize: 20),
                )
            ),
          ),
          new Card(
            margin: const EdgeInsets.all(50.0),
            child: Container(
                margin: const EdgeInsets.all(10.0),
                width: 300,
                height: 100,
                child: Text(
                  'あなたは世界で第${widget.myRank}位です！',
                  style: TextStyle(fontSize: 18),
                )
            ),
          ),
          new SizedBox(
            child:Padding(
              padding: const EdgeInsets.all(6.0),
              child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed("/home");
                          }, child: Icon(Icons.home),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () async {
                            await Navigator.of(context).pushReplacementNamed("/ready");
                          }, child: Icon(Icons.replay),
                        ),
                      ),
                    )
                  ]
              ),
            ),
            height: 60,
            width: 70,
          )
        ],
      ),
    );
  }
}

