import 'dart:async';
import 'package:flutter/material.dart';

import 'gameScreen.dart';

class ReadyScreen extends StatefulWidget {
  @override
  _ReadyScreenState createState() => _ReadyScreenState();
}
class _ReadyScreenState extends State<ReadyScreen> {
  static const int START = 1;
  int _seconds = START;
  bool _timerRunning = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _startTimer();
  }
  void _startTimer() {
    _timerRunning = true;
    _seconds = START;
    Timer.periodic(
      Duration(seconds: 1),
          (Timer timer) => setState(() {
        if (_seconds < 2) {
          _timerRunning = false;
          timer.cancel();
          Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                pageBuilder:
                    (context, animation, secondaryAnimation) => GameScreen(),
                transitionsBuilder: (context, animation,
                    secondaryAnimation, child) {
                  return FadeUpwardsPageTransitionsBuilder()
                      .buildTransitions(
                      MaterialPageRoute(
                          builder: (context) =>
                              GameScreen()),
                      context,
                      animation,
                      secondaryAnimation,
                      child);
                },
              ));
        } else {
          _seconds--;
        }
      }),
    );
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(" "),
        backgroundColor: Colors.black54,
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              _timerRunning ? '$_seconds' : '',
              style: TextStyle(fontSize: 200, fontWeight: FontWeight.w300),
            ),
            Visibility(
              child: SizedBox(
                height: 80,
                width: 130,

              ),
              visible: !_timerRunning,
            )
          ],
        ),
      ),
    );
  }
}