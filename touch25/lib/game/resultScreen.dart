import 'dart:math';

import 'package:flutter/material.dart';

import 'gameScreen.dart';


class ResultScreen extends StatelessWidget {

  var resultTime = 0;
  var myRank = 0;

  ResultScreen({Key? key, required this.resultTime, required this.myRank}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _num = resultTime / 100;
    return Scaffold(
      appBar: AppBar(
        title: Text("Result"),
        backgroundColor: Colors.black54,
        automaticallyImplyLeading: false,
      ),
      body: new ListView(
        children: <Widget>[
          new Card(
            margin: const EdgeInsets.all(50.0),
            child: Container(
                margin: const EdgeInsets.all(10.0),
                width: 300,
                height: 50,
                child: Text(
                  'あなたは${_num}秒です！',
                  style: TextStyle(fontSize: 25),
                )
            ),
          ),
          new Card(
            margin: const EdgeInsets.all(50.0),
            child: Container(
                margin: const EdgeInsets.all(10.0),
                width: 300,
                height: 100,
                child: Text(
                  'あなたは世界で第${myRank}位です！',
                  style: TextStyle(fontSize: 20),
                )
            ),
          ),
          new SizedBox(
            child:Padding(
              padding: const EdgeInsets.all(6.0),
              child:Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacementNamed("/home");
                          }, child: Icon(Icons.home),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ElevatedButton(
                          onPressed: () async {
                            await Navigator.of(context).pushReplacementNamed("/ready");
                            resultTime = 0;
                          }, child: Icon(Icons.replay),
                        ),
                      ),
                    )
                  ]
              ),
            ),
            height: 60,
            width: 70,
          )
        ],
      ),
    );
  }
}
