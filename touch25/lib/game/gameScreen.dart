import 'dart:async';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:touch25/game/resultScreen02.dart';
import 'package:touch25/game/resultScreen.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:device_info_plus/device_info_plus.dart';

import 'package:just_audio/just_audio.dart';

class GameScreen extends StatefulWidget {
  const GameScreen({Key? key}) : super(key: key);

  @override
  _GameScreenState createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  var gameTime = 0;
  var _target = 1;
  static const int _side = 2; // one side of square
  List<int> enable = List.generate(_side * _side, (_) => 1); // for button
  static final List<int> list = List<int>.generate(_side * _side, (i) => i + 1);
  static List<int> randomList = _shuffle(list);
  var numMatrix = List.generate(
      _side, (i) => List.generate(_side, (j) => randomList[i * _side + j]));
  late Timer _timer;
  List<double> myRecords = [];
  bool _timerRunning = false;
  var myRank = 0;
  late AudioPlayer player;

  @override
  void initState() {
    super.initState();
    startTimer();
    player = AudioPlayer();
  }

  @override
  void dispose() {
    player.dispose();
    super.dispose();
  }

  void startTimer() {
    setState(() {
      _timerRunning = true;
    });
    var oneSec = const Duration(milliseconds: 10);
    _timer = new Timer.periodic(
        oneSec,
        (Timer timer) => setState(() {
              gameTime++;
            }));
  }

  void stop() {
    setState(() {
      _timerRunning = false;
    });
    _timer.cancel();
  }

  void writeRecord() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();      // // AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    String userId = iosInfo.identifierForVendor ?? "";
    myRecords.clear();

    CollectionReference records = FirebaseFirestore.instance.collection('records');
    records.add({
      'user': userId,
      'time': gameTime / 100,
    });
    // Call the user's CollectionReference to add a new user
    // records
    //     .where('user', isEqualTo: userId)
    //     .orderBy('time')
    //     .limit(10)
    //     .get()
    //     .then((QuerySnapshot querySnapshot) {
    //   querySnapshot.docs.forEach((doc) {
    //     myRecords.add(doc["time"]);
    //     // print('userId: ' + doc["user"]);
    //     // print('time:  ' + doc["time"].toString());
    //   });
    // });

    var respectsQuery = records
        .where('time', isLessThan: gameTime / 100);
    var querySnapshot = await respectsQuery.get();
    myRank = querySnapshot.docs.length + 1;
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ResultScreen02(resultTime: gameTime / 100, myRank: myRank)
                // ResultScreen(resultTime: gameTime, myRank: myRank)
        )
    );

  }

  @override
  Widget build(BuildContext context) {
    double _num = gameTime / 100;
    return Scaffold(
      appBar: AppBar(
        title: Text("Tap 25"),
        backgroundColor: Colors.black54,
        automaticallyImplyLeading: false,
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 70,
                          child: Text(_num.toStringAsFixed(2),
                              style:
                                  TextStyle(fontSize: 20, color: Colors.black)),
                        ),
                        SizedBox(
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: Colors.redAccent,
                              ),
                              child: Text(_timerRunning ? "stop" : "start"),
                              onPressed: () {
                                _timerRunning ? stop() : startTimer();
                                if (_timerRunning == false) {
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return Center(
                                          child: Container(
                                        padding: EdgeInsets.all(30),
                                        width: 350,
                                        height: 500,
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: <Widget>[
                                            ElevatedButton(
                                              child: Icon(Icons.play_arrow),
                                              onPressed: () => {
                                                Navigator.pop(context),
                                                startTimer(),
                                              },
                                            ),
                                            ElevatedButton(
                                              onPressed: () => {
                                                stop(),
                                                Navigator.of(context)
                                                    .pushNamed("/ready")
                                              },
                                              child: Icon(Icons.replay),
                                            ),
                                          ],
                                        ),
                                      ));
                                    },
                                  );
                                }
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        ElevatedButton(
                          onPressed: () =>
                              {Navigator.of(context).pushNamed("/home")},
                          child: Icon(Icons.home),
                        )
                      ],
                    ))),
            for (var i = 0; i < _side; i++)
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  for (var j = 0; j < _side; j++) _buildButton(numMatrix[i][j]),
                ],
              )
          ]),
    );
  }

  Widget _buildButton(int index) {
    bool _flag = enable[index - 1] == 1;
    return SizedBox(
      child: Padding(
        padding: const EdgeInsets.all(6.0),
        child: ElevatedButton(
          onPressed: () async {
            if (_target == index) {
              if (_target == _side * _side) {
                stop();
                writeRecord();
              } else {
                await player.setAsset('assets/audio/p_sound.mp3');
                player.play();
                setState(() {
                  _target++;
                  enable[index - 1] = 0;
                });
              }
            }
          },
          style: ElevatedButton.styleFrom(
            primary: _flag ? Colors.orange : Colors.white54,
          ),
          child: Text(
            '${index}',
            style: TextStyle(
                fontSize: 20,
                color: Colors.white.withOpacity(_flag ? 1.0 : 0.3)),
          ),
        ),
      ),
      height: 60,
      width: 70,
    );
  }
}

List<int> _shuffle(List<int> items) {
  var random = new Random();
  for (var i = items.length - 1; i > 0; i--) {
    var n = random.nextInt(i + 1);
    var temp = items[i];
    items[i] = items[n];
    items[n] = temp;
  }
  return items;
}
